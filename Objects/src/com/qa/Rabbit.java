package com.qa;

public class Rabbit extends Animal {

	public Rabbit(String name, int age) {
		super(name, age);
	}
	
	public String toString(){
		return "Rabbit: " + super.toString();
	}
	
}
