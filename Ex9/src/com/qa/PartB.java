package com.qa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class PartB implements ActionListener {
	//run the program
	public static void main(String[] args) {
		new PartB();
	}

	private JFrame frame;
	private JTextArea textArea;

	public PartB() {
		//create the frame
		frame = new JFrame();
		frame.setSize(500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());

		// get the panel from the setupButtonPanel() method and
		// add it to the left of the screen
		frame.add(setupButtonPanel(), BorderLayout.WEST);
		frame.add(setupTextAreaPanel(), BorderLayout.CENTER);
		frame.setJMenuBar(setupMenu());

		frame.setVisible(true);

	}

	// Text area panel in the center
	private JPanel setupTextAreaPanel() {
		//create a new panel
		JPanel panel = new JPanel(new BorderLayout());
		textArea = new JTextArea();

		// load the file
		String file = loadFile("../Ex8/Output.txt");

		// set the text for the JTextArea
		textArea.setText(file);

		//add the text area to the panel
		panel.add(textArea, BorderLayout.CENTER);
		return panel;
	}

	// adapted from Ex8, loads a file
	private String loadFile(String file) {
		BufferedReader br = null;
		String str = "";

		try {
			br = new BufferedReader(new FileReader(file));
			String line = br.readLine();

			while (line != null) {
				str += line + "\n";
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return str;
	}

	//setup the menu bar with action events
	private JMenuBar setupMenu() {
		// menu bar
		JMenuBar menuBar = new JMenuBar();

		// file menu and items
		JMenu file = new JMenu("File");
		JMenuItem newMenuItem = new JMenuItem("New");
		newMenuItem.setActionCommand("new");
		newMenuItem.addActionListener(this);
		newMenuItem.setMnemonic(KeyEvent.VK_N);
		
		JMenuItem openMenuItem = new JMenuItem("Open");
		openMenuItem.setActionCommand("open");
		openMenuItem.addActionListener(this);
		openMenuItem.setMnemonic(KeyEvent.VK_O);
		
		JMenuItem saveMenuItem = new JMenuItem("Save");
		saveMenuItem.setActionCommand("save");
		saveMenuItem.addActionListener(this);
		saveMenuItem.setMnemonic(KeyEvent.VK_S);

		file.add(newMenuItem);
		file.add(openMenuItem);
		file.add(saveMenuItem);

		// help menu and items
		JMenu help = new JMenu("Help");
		JMenuItem aboutMenuItem = new JMenuItem("About");
		aboutMenuItem.setActionCommand("about");
		aboutMenuItem.addActionListener(this);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		help.add(aboutMenuItem);

		menuBar.add(file);
		menuBar.add(help);

		return menuBar;
	}

	//setup the button panel with action events
	private JPanel setupButtonPanel() {
		// create the panel
		JPanel buttonPanel = new JPanel(new GridLayout(9, 1));

		// create the buttons
		JButton newButton = new JButton("New");
		newButton.setActionCommand("new");
		newButton.addActionListener(this);
		newButton.setMnemonic(KeyEvent.VK_N);

		JButton openButton = new JButton("Open");
		openButton.setActionCommand("open");
		openButton.addActionListener(this);

		JButton saveButton = new JButton("Save");
		saveButton.setActionCommand("save");
		saveButton.addActionListener(this);

		// create the label
		JLabel colourLabel = new JLabel("Text Colour");

		// create the radio buttons
		JRadioButton red = new JRadioButton("Red");
		JRadioButton orange = new JRadioButton("Orange");
		JRadioButton blue = new JRadioButton("Blue");
		JRadioButton green = new JRadioButton("Green");
		JRadioButton pink = new JRadioButton("Pink");
//		red.addActionListener(e -> {textArea.setForeground(Color.RED);});
//		red.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				textArea.setForeground(Color.RED);
//			}
//		});
		red.addActionListener(this);
		orange.addActionListener(this);
		blue.addActionListener(this);
		green.addActionListener(this);
		pink.addActionListener(this);

		// add them all to one group so only one can be selected
		ButtonGroup group = new ButtonGroup();
		group.add(red);
		group.add(orange);
		group.add(blue);
		group.add(green);
		group.add(pink);

		// add all the componenents to the panel
		buttonPanel.add(newButton);
		buttonPanel.add(openButton);
		buttonPanel.add(saveButton);
		buttonPanel.add(colourLabel);
		buttonPanel.add(red);
		buttonPanel.add(orange);
		buttonPanel.add(blue);
		buttonPanel.add(green);
		buttonPanel.add(pink);

		return buttonPanel;
	}

	//save the file - called by the action performed method
	private void save() {
		//open a file chooser
		JFileChooser fc = new JFileChooser("Save");
		fc.showSaveDialog(frame);
		//get the file selected by the user
		File f = fc.getSelectedFile();

		//write to this file
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(f));
			bw.write(textArea.getText());
			JOptionPane.showMessageDialog(frame, "File Saved!");
		} catch (IOException e) {
			//auto generated code
			e.printStackTrace();
		} finally {
			//close the file
			try {
				if (bw != null)
					bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//change the text colour when the radio button is changed
	private void changeColour(String ae) {
		Color c = null;
		//could also be done with an if..else if..else construct
		switch (ae) {
		case "Red":
			c = Color.RED;
			break;
		case "Orange":
			c = Color.ORANGE;
			break;
		case "Green":
			c = Color.GREEN;
			break;
		case "Blue":
			c = Color.BLUE;
			break;
		case "Pink":
			c = Color.MAGENTA;
			break;
		default:
			c = Color.BLACK;

		}
		//set the text to this colour
		textArea.setForeground(c);
	}

	//create an about frame from the help menu
	private void aboutFrame() {
		
		//create a second jframe, it must be final so the anon inner class can use it
		final JFrame about = new JFrame();
		about.setSize(400, 200);
		//dispose rather than exit on close, this removes the current frame
		//from memory, but doesn't exit everything
		about.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		about.setLayout(new BorderLayout());

		//add a content panel
		JPanel content = new JPanel(new BorderLayout());

		//add some text to the panel and make it not editable
		JTextArea aboutText = new JTextArea();
		aboutText
				.setText("This is all about the program. \n This is a quick example created for the course.");
		aboutText.setEditable(false);

		//add a close button, we could handle the event in the actionPerformed method
		//in this class, but as its a single event that is not to do with the main
		//frame it is better to keep it separate. Otherwise we need to expose the 
		//about object globally
		JButton close = new JButton("Close");
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				about.dispose();
			}
		});

		//add all the content
		content.add(aboutText, BorderLayout.CENTER);
		content.add(close, BorderLayout.SOUTH);

		about.add(content, BorderLayout.CENTER);

		about.setVisible(true);
	}

	//action performed method, interprets what event happens and calls the right method
	@Override
	public void actionPerformed(ActionEvent ae) {
		//new button or menu event
		if (ae.getActionCommand().equals("new")) {
			System.out.println("Clicked New Button");
			textArea.setText("");
		} else if (ae.getActionCommand().equals("save")) {
			//save button or menu event
			System.out.println("Saving file");
			save();
		} else if (ae.getActionCommand().equals("open")) {
			//open button or menu event
			System.out.println(ae.getActionCommand());

			//get the file to open
			JFileChooser fc = new JFileChooser("Save");
			fc.showSaveDialog(frame);
			File f = fc.getSelectedFile();

			//use the existing load method in the code
			String fileContents = loadFile(f.toString());

			//set the text area
			textArea.setText(fileContents);

		} else if (ae.getActionCommand().equals("about")) {
			//open the about frame
			System.out.println(ae.getActionCommand());
			aboutFrame();
		} else {
			//change the text colour
			changeColour(ae.getActionCommand());
		}
	}
}
