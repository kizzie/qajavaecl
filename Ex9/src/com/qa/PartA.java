package com.qa;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class PartA {
	public static void main(String[] args) {
		new PartA();
	}
	
	public PartA(){
		JFrame frame = new JFrame();
		frame.setSize(500,500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		//get the panel from the setupButtonPanel() method and 
		//add it to the left of the screen
		frame.add(setupButtonPanel(), BorderLayout.WEST);
		frame.add(setupTextAreaPanel(), BorderLayout.CENTER);
		frame.setJMenuBar(setupMenu());
		
		frame.setVisible(true);
		
	}
	
	public JPanel setupTextAreaPanel(){
		JPanel panel = new JPanel(new BorderLayout());
		JTextArea textArea = new JTextArea();
		
		//load the file
		String file = loadFile();
		
		//set the text for the JTextArea
		textArea.setText(file);
		
		panel.add(textArea, BorderLayout.CENTER);
		return panel;
	}
	
	//adapted from Ex8
	public String loadFile(){
		BufferedReader br = null;
		String str = "";
		
		try {
			br = new BufferedReader(new FileReader("../Ex8/Output.txt"));
			String line = br.readLine();
			
			while (line != null){
				str += line + "\n";
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str;
	}
	
	public JMenuBar setupMenu(){
		//menu bar
		JMenuBar menuBar = new JMenuBar();
		
		//file menu and items
		JMenu file = new JMenu("File");
		JMenuItem newMenuItem = new JMenuItem("New");
		JMenuItem openMenuItem = new JMenuItem("Open");
		JMenuItem saveMenuItem = new JMenuItem("Save");
		file.add(newMenuItem);
		file.add(openMenuItem);
		file.add(saveMenuItem);
		
		//help menu and items
		JMenu help = new JMenu("Help");
		JMenuItem aboutMenuItem = new JMenuItem("About");
		help.add(aboutMenuItem);
		
		menuBar.add(file);
		menuBar.add(help);
		
		return menuBar;		
	}

	public JPanel setupButtonPanel(){
		//create the panel
		JPanel buttonPanel = new JPanel(new GridLayout(9,1));
		
		//create the buttons
		JButton newButton = new JButton("New");
		JButton openButton = new JButton("Open");
		JButton saveButton = new JButton("Save");
		
		//create the label
		JLabel colourLabel = new JLabel("Text Colour");
		
		//create the radio buttons
		JRadioButton red = new JRadioButton("Red");
		JRadioButton orange = new JRadioButton("Orange");
		JRadioButton blue = new JRadioButton("Blue");
		JRadioButton green = new JRadioButton("Green");
		JRadioButton pink = new JRadioButton("Pink");		
		
		//add them all to one group so only one can be selected
		ButtonGroup group = new ButtonGroup();
		group.add(red);
		group.add(orange);
		group.add(blue);
		group.add(green);
		group.add(pink);
		
		//add all the componenents to the panel
		buttonPanel.add(newButton);
		buttonPanel.add(openButton);
		buttonPanel.add(saveButton);
		buttonPanel.add(colourLabel);
		buttonPanel.add(red);
		buttonPanel.add(orange);
		buttonPanel.add(blue);
		buttonPanel.add(green);
		buttonPanel.add(pink);
		
		return buttonPanel;
	}
}
