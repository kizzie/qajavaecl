package com.qa;

public class Book {
	
	private String name;
	private String[] authors;
	private double price;

	//2
	public Book(String name, String[] authors, double price) {
		this.name = name;
		this.authors = authors;
		this.price = price;
	}

	//3
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getAuthors() {
		return authors;
	}

	public void setAuthors(String[] authors) {
		if (authors.length <= 5) {
			this.authors = authors;
		} else {
			System.out
					.println("Error, you can only have up to five authors. Change not made");
		}
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	//4
	public void addAuthor(String author) {
		if (authors.length <= 5) {
			authors[authors.length] = author;
		} else {
			System.out
					.println("Error, you can only have up to five authors. Change not made");
		}
	}

	//5
	public String toString(){
		String str = "Book: [" + name + ", ";
		
		for(int x = 0; x < authors.length; x ++){
			str += authors[x] + ", ";
		}
		
		str += price + "]";
		
		return str;
	}
	

}
