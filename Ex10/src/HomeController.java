

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.User;

/**
 * Servlet implementation class HomeController
 */
@WebServlet("/")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeController() {

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processPage(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processPage(request, response);
	}
	
	private void processPage(HttpServletRequest request, HttpServletResponse response){
		try {
			response.getWriter().write("Hello world");
			
			//create a user
			User user = new User(0, "Alice", "Alice@Email.com");
			
			ArrayList<User> userList = new ArrayList<>();
			userList.add(user);
			userList.add(new User(1, "Bob", "Bob@email.com"));
			userList.add(new User(2, "Mallory", "Mal@email.com"));
			userList.add(new User(3, "Eve", "eve@email.com"));
			
			//pass it to the JSP page
			request.setAttribute("user", user);
			request.setAttribute("userList", userList);
			
			//output the JSP page
			request.getRequestDispatcher("/test.jsp").forward(request, response);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
