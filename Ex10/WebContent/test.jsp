<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>This is a page all about a User</h1>
	<p>ID : ${user.getUserID()}</p>
	<p>Name : ${user.getName()}</p>
	<p>Email:${user.getEmail() }</p>
</body>

<!--  if we are using JSTL -->

<h2>All the users in the list using JSTL</h2>

<c:forEach var="u" items="${userList}">
	<p>ID : ${u.getUserID()}</p>
	<p>Name : ${u.getName()}</p>
	<p>Email:${u.getEmail() }</p>
</c:forEach>

</html>