package com.qa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class files {

	public static void main(String[] args) {
		try {
			new files().readFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadLineException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			System.out.println(e.getBadLine());
		}
	}

	public files() {}

	public void readFile() throws BadLineException, FileNotFoundException, IOException {
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(new File("input.txt")));

			String line = br.readLine();

			while (line != null) {
				if (line.equals("nope")) { throw new BadLineException("Line said nope", line); } 
				System.out.println(line);
				br.readLine();
			}
		} finally {
			System.out.println("Doing the finally clause before returning");
			if (br != null)
				br.close();
		}
	}
}
