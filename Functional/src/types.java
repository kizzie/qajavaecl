import java.util.function.Consumer;
import java.util.function.DoubleFunction;
import java.util.function.DoubleToIntFunction;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.Supplier;


public class types {

	public static void main(String[] args) {
		new types();
	}
	
	public types(){
		
		Consumer<String> func1 = s -> System.out.println(s);
		
		IntBinaryOperator func2 = (int x, int y) -> { return x + y; };
		
		Supplier<String> func3 = () -> "Hello World";

		DoubleFunction<Double> func4 = r -> Math.PI * r * r;

		DoubleToIntFunction func5 = (double d) -> (int) d;
		
		Function<Integer, Integer> func6 = (Integer i) -> i = i + 1;
		
		
	}
}
