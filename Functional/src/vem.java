

public class vem {

	public static void main(String[] args) {
		new vem();
	}
	
	public vem(){
		foo f = new foo();
		f.doStuff();
		
		bar b = new bar();
		b.doStuff();
		
		a a = new a();
		a.write("cookies");
	}
	
	public class a implements prettyWriter, UnrelatedInterface {
		public void write(String msg){
			UnrelatedInterface.super.write(msg);
		}
	} 
	
	public class bar extends foo implements prettyWriter {}
	
	public class foo implements writer{
		public void doStuff(){
			write("Hello World");
		}
	} 
	
	public interface writer {
		public default void write(String msg) { System.out.println(msg); }
	}
	
	public interface prettyWriter extends writer {
		@Override
		public default void write(String msg){ 
			System.out.println("****" + msg + "+++++"); 
		}
	}
	
	public interface UnrelatedInterface {
		public default void write(String msg){
			System.out.println("I refuse to print: " + msg);
		}
	}
	
	

}
