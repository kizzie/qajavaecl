import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class func {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("a");
		list.add("bb");
		list.add("ccc");
		list.add("d");
		
		list.forEach(s -> {System.out.println(s);});
		
		String concatenated = list.stream().reduce("", String::concat);
		System.out.println(concatenated);
		
		ArrayList<Integer> intList = new ArrayList(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
		
		System.out.println(intList.stream().reduce(1, (x, a) -> a * x));
		
		intList.forEach(i -> i = i + 1);
		
		intList.forEach(i -> System.out.println(i));
		
		ArrayList<Integer> tempList = new ArrayList<Integer>();
		for (int x = 0; x < intList.size(); x++){
			tempList.add(intList.get(x) + 1);
		}
		intList = tempList;
		
		Stream<Integer> newList = intList.stream().map(i -> i * 2);
		
//		Stream<Boolean> isEven = intList.stream().map(i -> i % 2 == 0);
		Stream<Integer> isEven = intList.stream().filter(i -> i % 2 == 0);
		isEven.forEach(s -> System.out.print(s + ","));
//		pie.forEach(i -> System.out.println(i));
//		Optional<Integer> result = newList.reduce((Integer x, Integer y) -> {return x + y;});
//		System.out.println(result);
		
//		intList.stream().map(i -> i * i).filter(i -> i % 2 == 0).forEach(System.out::println);
//		
//		List<Integer> mapFilterList = intList.stream()
//						.map(i -> i * i)
//						.filter(i -> i % 2 == 0)
//						.collect(Collectors.toList());
//		
//		IntStream intStream2 = IntStream.range(1,10);
//		
//		
//		String total = intList.stream()
//				.map(i -> i * i)
//				.filter(i -> i % 2 ==0)
//				.collect(Collectors.summarizingInt(i -> i)).toString();
//		System.out.println("Statistics: " + total);
//		
//		mapFilterList.forEach(s-> System.out.print(s + ","));
//		callMethod(list, s -> System.out.println(s));
//		
//		
//		List<String> myList =
//			    Arrays.asList("a1", "a2", "b1", "c2", "c1");
//
//			myList
//			    .stream()
//			    .filter(s -> s.startsWith("c"))
//			    .map(String::toUpperCase)
//			    .sorted()
//			    .forEach(System.out::println);
//
//		List<String> newList = 
//				list.stream().map(s -> {return s.toUpperCase();})
//					.collect(Collectors.toList());
//		
//		intList.stream().map(i -> i = i * 2);
//		intList.forEach(System.out::println);
	}
	
	
	
	private static void callMethod(ArrayList<String> string, Consumer<? super String> p) {

//		Stream<char[]> str = Arrays.asList(string.toCharArray()).stream();
		string.forEach(p);
//		str.forEach(p);
	}

	private static void callMethod(ArrayList<String> list, Object object) {
		// TODO Auto-generated method stub
		

	}
}


