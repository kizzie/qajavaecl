import java.util.ArrayList;
import java.util.Arrays;


public class Recursion {

	public static void main(String[] args) {
		new Recursion();
	}
	
	static int factorial;
	
	public Recursion(){
		ArrayList list = new ArrayList<String>(Arrays.asList(new String[] {"a", "b", "c", "d"}));
		
		System.out.println(recursiveMethod(list, "c", 0));
		
	}
	
	public int recursiveMethod(ArrayList<String> list, String lookingFor, int currentPosition){
		
		if (list.get(0).equals(lookingFor)){
			return currentPosition;
		} else {
			if (list.isEmpty()){
				return -1;
			} else {
				list.remove(0);
				currentPosition = recursiveMethod(list, lookingFor, currentPosition+1);
			}
		}
		
		return currentPosition;
		
		
	}
	
	
}
