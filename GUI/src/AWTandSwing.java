import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


public class AWTandSwing {

	public static void main(String[] args) {
//		Frame f = new Frame("AWT Frame");
//		f.setSize(500, 500);
//		f.setVisible(true);
//		
//		f.addWindowListener(new WindowAdapter() {
//			public void windowClosing(WindowEvent we) {
//				System.exit(0);
//			}
//		}
//		);
//		f.setLayout(new GridLayout(10,1));
//		Button b = new Button("Button1");
//		Button b2 = new Button("Button2");
//		Button b3 = new Button("Button3");
//		Button b4 = new Button("Button4");
//		f.add(b);
//		f.add(b2);
//		f.add(b3);
//		f.add(b4);
		
		JFrame f2 = new JFrame("Swing Frame");
		f2.setSize(500, 500);
		f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
//		JButton button1 = new JButton("Button1");
//		JButton button2 = new JButton("Button2");
//		JButton button3 = new JButton("Button3");
//		JButton button4 = new JButton("Button4");
//		JButton button5 = new JButton("Button5");
//		panel.add(button1, "North");
//		panel.add(button2, BorderLayout.SOUTH);
//		panel.add(button3, BorderLayout.WEST);
//		panel.add(button4, BorderLayout.EAST);
//		panel.add(button5, BorderLayout.CENTER);
		
//		GridBagConstraints gbc = new GridBagConstraints();
//		gbc.gridx = 0;
//		gbc.gridy = 0;
//		gbc.fill = GridBagConstraints.HORIZONTAL;
//        gbc.anchor = GridBagConstraints.CENTER;
//		panel.add(button1, gbc);
//		gbc.gridx = 1;
//		gbc.gridy = 1;
//		gbc.gridheight = 2;
//		gbc.fill = GridBagConstraints.HORIZONTAL;
//		panel.add(button2, gbc);
//		gbc.gridx = 0;
//		gbc.gridy = 2;
//		panel.add(button3, gbc);
//		gbc.gridx = 3;
//		gbc.gridy = 3;
//		panel.add(button4, gbc);
//		gbc.gridx = 5;
//		gbc.gridy = 4;
//		panel.add(button5, gbc);
		
		
		//labels and text fields
		JPanel centerPanel = new JPanel(new GridLayout(0, 2));
		
		JLabel nameLabel = new JLabel("Name");
//		nameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		centerPanel.add(nameLabel);
		JTextField nameBox = new JTextField();
		centerPanel.add(nameBox);
		
		centerPanel.add(new JLabel("Email Address"));
		JTextField emailBox = new JTextField();
		centerPanel.add(emailBox);
		
		centerPanel.add(new JLabel("Age"));
		JTextField age = new JTextField();
		centerPanel.add(age);
		
		JPanel leftPanel = new JPanel(new BorderLayout());
		String[] animals = {"Cat", "Dog", "Rabbit", "Fish"};
		JList<String> list = new JList<String>(animals);
		list.setFont(new Font("", 0, 40));
		leftPanel.add(list, BorderLayout.CENTER);
		
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.setFont(new Font("", 0, 40));
		tabbedPane.add("Text Fields", centerPanel);
		tabbedPane.add("List", leftPanel);
		
//		JSplitPane split = new JSplitPane();
//		split.setDividerLocation(200);
//		split.setLeftComponent(leftPanel);
//		split.setRightComponent(centerPanel);
//		tabbedPane.add("Split", split);
		
		JPanel radioPanel = new JPanel(new GridLayout(10,2));
		
		JRadioButton b1 = new JRadioButton("Option 1");
		JRadioButton b2 = new JRadioButton("Option 2");
		JRadioButton b3 = new JRadioButton("Option 3");
		JRadioButton b4 = new JRadioButton("Option 4");
		
		ButtonGroup group = new ButtonGroup();
		group.add(b1);
		group.add(b2);
		group.add(b3);
		group.add(b4);
		
		radioPanel.add(b1);
		radioPanel.add(b2);
		radioPanel.add(b3);
		radioPanel.add(b4);
		
		tabbedPane.add("RadioButtons", radioPanel);
		
		//graphics panel
		tabbedPane.add("Drawing", new MyPanel());
		
//		panel.add(centerPanel, BorderLayout.WEST);
		panel.add(tabbedPane, BorderLayout.CENTER);
		
		
		
		
		f2.add(panel);
		
		f2.setVisible(true);
		
	}
	
	
	
}
