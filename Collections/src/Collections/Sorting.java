package Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class Sorting {

	public static void main(String[] args) {
		int[] intArr = {11,53,1,2,6,22,99};
		
		Arrays.sort(intArr);
		
		for (int x = 0; x < intArr.length; x ++){
			System.out.print(intArr[x] + ", ");
		}
		
		System.out.println();
		
		ArrayList<Integer> intList = new ArrayList<Integer>();
		
		for (int x = 0; x < intArr.length; x ++){
			intList.add(intArr[x]);
		}
		
		Collections.sort(intList);
		
		for (int x = 0; x < intList.size(); x ++){
			System.out.print(intList.get(x) + ", ");
		}
		
		System.out.println();
		
		ArrayList<BookWithComparitor> bookList = new ArrayList<>();
		bookList.add(new BookWithComparitor("Book1", new String[5], 10.99));
		bookList.add(new BookWithComparitor("Book2", new String[5], 1.99));
		bookList.add(new BookWithComparitor("Book3", new String[5], 21.99));
		bookList.add(new BookWithComparitor("Book4", new String[5], 99.99));
		
		Collections.sort(bookList);
		
		for (BookWithComparitor b : bookList){
			System.out.println(b);
		}
		
		ArrayList<Book> bookList2 = new ArrayList<>();
		bookList2.add(new Book("Book1", new String[5], 10.99));
		bookList2.add(new Book("Book2", new String[5], 1.99));
		bookList2.add(new Book("Book3", new String[5], 21.99));
		bookList2.add(new Book("Book4", new String[5], 99.99));
		
		Collections.sort(bookList2, new CompareBooks());
		
		for (Book b : bookList2){
			System.out.println(b);
		}
		
	}
}
