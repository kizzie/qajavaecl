package Collections;
import java.util.Comparator;


public class CompareBooks implements Comparator {

	@Override
	public int compare(Object arg0, Object arg1) {
		
		Book thiss = (Book) arg0;
		Book that = (Book) arg1;
		
		if (thiss.getPrice() < that.getPrice()){
			return -1;
		} else if (thiss.getPrice() > that.getPrice()){
			return 1;
		} else {
		//if this equals that
			return 0;
		}
	}

}
	