package Collections;
import java.util.HashMap;
import java.util.Iterator;


public class HashMapExample {

	public static void main(String[] args) {
		HashMap<String, Book> bookMap = new HashMap<>();
		
		bookMap.put("First", new Book("Book1", new String[5], 10.99));
		bookMap.put("Second", new Book("Book2", new String[5], 13.99));
		bookMap.put("Third", new Book("Book3", new String[5], 16.99));
		
		for (Book b : bookMap.values()){
			System.out.println(b);
		}
		
		System.out.println("Just the second book: ");
		System.out.println(bookMap.get("Second"));
		
		System.out.println("With an iterator based on the values!");
		Iterator<Book> iter = bookMap.values().iterator();
		
		while (iter.hasNext()){
			System.out.println(iter.next());
		}
		
	}
}
