package Collections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Arraylist {
	
	public static void main(String[] args) {
		Book b = new Book("Amazing Book", new String[5], 10.00);
		
		ArrayList<Book> bookList = new ArrayList<>();		
		bookList.add(new Book("Title", new String[5], 19.00));
		bookList.add(b);
		
		
		
		for (Book book : bookList){
			if (book.getName().equals("Amazing Book")) {
				System.out.println("Index of Amazing Book: " + bookList.indexOf(book));
			}
		}
		
		
		
		Iterator<Book> iter = bookList.iterator();
		
		while (iter.hasNext()){
			System.out.println(iter.next());
		}
		
		
		
		
		
		
	}

}
