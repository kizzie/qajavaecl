package Animals;

public class AnimalGeneric<T extends Animal> {
	
	T animal;
	
	public AnimalGeneric(T animal){
		this.animal = animal;
	}
	
	public T getAnimal(){
		return animal;
	}
}
