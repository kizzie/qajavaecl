package Animals;

import Collections.Book;

public class GenericObject<T> {
	
	T genObject;
	
	public GenericObject(T genObject){
		this.genObject = genObject;
	}
	
	public void setObject(T genObject){
		this.genObject = genObject;
	}
	
	public T getObject(){
		return genObject;
	}
	
	public <V> void doSomething(V anotherObject){
		System.out.println(genObject + ", " + anotherObject);
	}

	
	public static void main(String[] args) {
		GenericObject<String> obj1 = new GenericObject("Hi");
		GenericObject<Integer> obj2 = new GenericObject(55);
		GenericObject<Book> obj3 = new GenericObject(new Book("Book1", new String[5], 1.00));
		
		System.out.println(obj1.getObject());
		System.out.println(obj2.getObject());
		System.out.println(obj3.getObject());
		
		obj1.doSomething(new Integer(5));
		obj1.doSomething(new Animal("Peter", 2));
		
		
		AnimalGeneric<Rabbit> an1 = new AnimalGeneric<Rabbit>(new Rabbit("Peter", 2, true));
		an1.getAnimal().move();
		
		AnimalGeneric<Animal> an2 = new AnimalGeneric<Animal>(new Rabbit("Benjy", 3, true));
//		an2.getAnimal()).move();
		
//		AnimalGeneric<Book> an3 = new AnimalGeneric<Book>(new Book("", new String[5], 0.00));
		
		
		
	}
}
