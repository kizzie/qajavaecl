package Animals;

public class Rabbit extends Animal {
	
	boolean fuzzy;
	
	public Rabbit(String name, int age, boolean fuzzy){
		super(name, age);
		this.fuzzy = fuzzy;
	}
	
	public void move(){
		System.out.println("Hop!");
	}

}
