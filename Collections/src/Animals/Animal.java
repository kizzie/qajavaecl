package Animals;

public class Animal {

	private String name;
	private int age;
	
	public Animal(String name, int age){
		this.age = age;
		this.name = name;
	}
	
	public String toString(){
		return "Name: " + name + " Age: " + age;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setAge(){
		this.age = age;
	}
	
	public int getAge(){
		return age;
	}
	
}
