package com.qa;

public class Circle extends Shape implements Movable{

	
	double radius;
	
	public Circle(String name, String colour, double x, double y, double radius) {
		super(name, colour, x, y);
		this.radius = radius;
	}

	@Override
	public double getArea() {
		return Math.PI * radius * radius;
	}

	@Override
	public Point getCenterPoint() {
		return new Point(super.getX(), super.getY());
	}

	@Override
	public void move(double x, double y) {
		super.setX(super.getX() + x);
		super.setY(super.getY() + y);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", getArea()=" + getArea()
				+ ", getCenterPoint()=" + getCenterPoint() + ", getX()="
				+ getX() + ", getY()=" + getY() + ", getName()=" + getName()
				+ ", getColour()=" + getColour() + "]";
	}

	@Override
	public Point getCurrentLocation() {
		return new Point(super.getX(), super.getY());
	}

	
}
