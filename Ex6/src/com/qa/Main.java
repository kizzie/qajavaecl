package com.qa;

public class Main {

	public static void main(String[] args) {
		//create some objects
		Circle c = new Circle ("circle1", "red", 0,0, 10);
		Circle c2 = new Circle ("circle2", "blue", 10,10, 5);
		Rectangle r = new Rectangle ("rectangle1", "yellow", 0, 0, 10, 10);
		Rectangle r2 = new Rectangle ("rectangle2", "green", 2, 2, 5, 2);
		
		//print out the objects
		System.out.println(c);
		//move c
		c.move(15, 15);
		System.out.println(c);
		System.out.println(c2);
		
		System.out.println(r);
//		r.move(15,15); //won't work as r doesn't implement movable
		System.out.println(r2);
		
	}
}
