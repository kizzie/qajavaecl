package com.other;

public class Test {

	public static void main(String[] args) {
		Dog d = new Dog("Spot", 2);
		
		System.out.println(d.getClass());
		
		if (d instanceof Dog){
			System.out.println("Its a dog");
		}
		
		if (d instanceof GuideDog) {
			System.out.println("It's a Guide Dog!");
		}
	}
}
