package com.qa;

public abstract class Shape {
	public abstract double getArea();
}
