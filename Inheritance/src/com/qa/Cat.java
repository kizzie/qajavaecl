package com.qa;

public class Cat extends Animal{
	
	boolean fluffy;
	
	public Cat(String name, int age, boolean fluffy){
		super(name, age);		
		this.fluffy = fluffy;
	}

	public boolean isFluffy(){
		System.out.println(name);
		return fluffy;
	}

	@Override
	public String sayHello() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String move() {
		// TODO Auto-generated method stub
		return null;
	}

}
