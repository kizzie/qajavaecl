package com.qa;

public class Circle extends Shape {
	@Override
	public double getArea() {
		return Math.PI * Math.pow(r, 2);
	}
}
