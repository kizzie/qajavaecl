package puzzle;

import java.util.Set;
import java.util.TreeSet;

public class main {

	public static void main(String[] args) {
		
		//a + 13(b/c)+d+12e-f+g(h/i)=87
		
		int solutions = 0;
		
		for (double a = 1; a < 10; a++){
			for (double b = 1; b < 10; b++){
				for (double c = 1; c < 10; c++){
					for (double d = 1; d < 10; d++){
						for (double e = 1; e < 10; e++){
							for (double f = 1; f < 10; f++){
								for (double g = 1; g < 10; g++){
									for (double h = 1; h < 10; h++){
										for (double i = 1; i < 10; i++){
//											System.out.println(a + ", " + b + ", " + c + ", " + d + ", " + e + ", " + f + ", " + g + ", " + h + ", " + i );
//											System.out.println(notEqual(a,b,c,d,e,f,g,h,i));
											if (notEqual(a,b,c,d,e,f,g,h,i)){
//												System.out
//														.println(a + (13*(b/c))+d+(12*e)-f+(g*(h/i)));
												if (a + (13*(b/c))+d+(12*e)-f+(g*(h/i)) == 87){
													System.out.println(a + ", " + b + ", " + c + ", " + d + ", " + e + ", " + f + ", " + g + ", " + h + ", " + i );
													solutions++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		System.out.println(solutions);
	}

	public static boolean notEqual(double a,double b,double c,double d,double e,double f,double g,double h,double i){
		Set s = new TreeSet();
		s.add(a);
		s.add(b);
		s.add(c);
		s.add(d);
		s.add(e);
		s.add(f);
		s.add(g);
		s.add(h);
		s.add(i);
		
		return (s.size() == 9);
	}
}
