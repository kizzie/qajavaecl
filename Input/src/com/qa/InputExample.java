package com.qa;

import java.io.IOException;
import java.util.Scanner;

public class InputExample {

	public static void main(String[] args) {
		
		
		try {
			System.out.println("Type a character");
			int i = System.in.read();
			System.out.println("You typed " + i);
			
			System.out.println("Now try a word");
			Scanner s = new Scanner(System.in);
			String line = s.next();
			System.out.println("You typed: " + line);
			
			System.out.println("Keep going! (-1 to stop)");
			
			String str = s.next();
			while (! str.equals("-1")) {
				System.out.println("Echo: " + str);
				str = s.next();
			}
			
			System.out.println("Finished");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
}
