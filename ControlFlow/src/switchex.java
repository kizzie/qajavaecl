public class switchex {
	public static void main(String[] args) {
		int i = 5;

		switch (i) {
		case 1:
			System.out.println("i is one");
			break;
		case 2:
			System.out.println("i is two");
			break;
		default:
			System.out.println("i was neither one or two");
		}
		
		String name = "alice";
		
		switch (name){
		case "alice":
			System.out.println("Hello Alice");
			break;
		case "bob":
			System.out.println("Hello Bob");
			break;
		} 
		
		
		
	}
}
