
public class Nesting {
	
	public static void main(String[] args) {
		
		int i = 0;
		int j = 0;
		
		while (i < 10){
			while (j < 10){
				System.out.println("(" + i + ", " + j + ")");
				j++;
			}
			j = 0;
			i++;
		}
		
	}

}
